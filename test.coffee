chai = require 'chai'
expect = chai.expect

RssBuilder = require './src/rss-builder'
builder = new RssBuilder()

describe 'Parse rss', ->
  before (done) ->
    builder.parseRss().then (data) ->
      done()

  it 'RSSのタイトルが「スラド」であること', ->
    expect(builder.meta.title).equal 'スラド'

  it '記事が10件であること', ->
    expect(builder.items.length).equal 10

  it 'コピーライトが正しいこと', ->
    expect(builder.meta.copyright).equal 'Copyright (C) スラド, OSDN Corporation'

  it 'コメントを取れること', (done) ->
    builder.getComments('http://science.srad.jp/story/16/04/15/2016231/').then (comments) ->
      console.log comments
      expect(1+1).equal 2
      done()

