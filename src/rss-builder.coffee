request = require 'request'
FeedPerser = require 'feedparser'
cheerio = require 'cheerio'
rss = require 'node-rss'

commentStyle = "<style>.comment-item{border:solid 1px #eee;margin:5px} li{list-style:none} blockquote{color:#aaa;font-style:italic}</style>"

class App
  url: 'http://rss.rssad.jp/rss/slashdot/slashdot.rss'

  parseRss: (rss = null) ->
    items = []
    feedparser = new FeedPerser()
    feedparser.on 'readable', ->
      while item = feedparser.read()
        items.push item

    new Promise (resolve, reject) =>
      req = request @url
      req.on 'response', (res) ->
        stream = this
        stream.pipe feedparser
      req.on 'end', =>
        @items = items
        @meta = feedparser.meta
        resolve
          items: @items
          meta: feedparser.meta

  getComments: (url) ->
    new Promise (resolve) ->
      req = request url, (err, res, body) ->
        $ = cheerio.load body, {decodeEntities: false}
        $('.details').remove()
        $('.commentSub').remove()
        $('.score').remove()
        $('.hide').remove()
        $('.comment .comment .title').remove()
        $('.title > h4 > a').each (idx, elem) ->
          $(elem).replaceWith $(elem).text()

        resolve commentStyle + $('#commentlisting').html()

  create: ->
    new Promise (resolve) =>
      feed = rss.createNewFeed('スラド with コメント', @meta.link, @meta.description, @meta.author)
      @items.forEach (item) ->
        feed.addNewItem(item.title, item.link, item.pubDate, item.description)
  
      resolve rss.getFeedXML(feed).replace /[\r\n]/g, ''

  insertCommentsAll: ->
    promise = Promise.resolve()
    @items.forEach (item, idx) =>
      promise = promise.then =>
        @getComments(item.link).then (comments) =>
          @items[idx].description = (item.description + comments)
    return promise

  build: ->
    @parseRss().then(@insertCommentsAll.bind(@)).then(@create.bind(@))

module.exports = App
