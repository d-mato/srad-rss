app = require('express')()
fs = require 'fs'

RssBuilder = require './src/rss-builder'

feedfile = __dirname+'/feed.xml'

app.get '/update', (req, res) ->
  res.send 'Updating...'
  builder = new RssBuilder()
  builder.build().then (rss) ->
    fs.writeFile feedfile, rss

app.get '/feed', (req, res) ->
  res.sendFile feedfile

app.listen(process.env.PORT || 3333)
